Darryl's Custom Gentoo Layman Overlay
=====================================

To add this to your list of overlays, as root, run the following

```
# Add the dsokoloski overlay to layman
layman -o https://bitbucket.org/dsokoloski/gentoo-overlay/raw/master/dsokoloski.xml -a dsokoloski

# Sync the dsokoloski layman overlay
layman -s dsokoloski

```

If you don't have layman setup on your machine please read this: https://www.gentoo.org/proj/en/overlays/userguide.xml

